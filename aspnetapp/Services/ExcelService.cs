using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using aspnetapp.Models;

namespace aspnetapp{

    /*
        Clase con los metodos necesarios que usaran los endpoint
     */
    public class ExcelService{

        public ExcelValidateResponse ReadExcel(IFormFile  Files)
        {
            var resValidate = ValidateExcel(Files);

            if(resValidate.Errors.Count > 0){
                return ExcelValidateResponse.GetResult(400, "Archivo con Errores", resValidate.Errors);
            }else{
                var list = new List<Validation>();
                return ExcelValidateResponse.GetResult(200, "OK", list);
            }
        }

        private ExcelValidateResponse ValidateExcel(IFormFile  Files)
        {
            var result = new ExcelValidateResponse();
            return result;
        }
    }
}