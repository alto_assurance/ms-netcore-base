using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace aspnetapp.Controllers{

    [SwaggerTag("Definicion API Lectura Excel")]
    [Route("api/test/[controller]")]
    [ApiController]
    /*
        clase que define los endpoint y documenta cada uno para visualizacion en swagger
    */
    public class ExcelController : ControllerBase{

        private readonly ExcelService _excelService;

        public ExcelController(ExcelService excelService)
        {
            _excelService = excelService;
        }

        [HttpPost("/{account_id}/readexcel")]
        [Produces("application/json")]
        [SwaggerOperation(
            Summary = "Recibe Archivo Excel",
            Description = "Procesa Excel Con Detalles de Los Viajes a Cargar por Cliente",
            OperationId = "ReadExcel"
        )]
        public ExcelValidateResponse ReadExcel([FromForm] ArchivoExcel  excel, string account_id)
        {
            return _excelService.ReadExcel(excel.File);
        }

    }
}