using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace aspnetapp.Models
{
    public class Author
    {
        [BsonElement("name")]
        public string name { get; set; }

        [BsonElement("lastname")]
        public string LastName { get; set; }

    }
}