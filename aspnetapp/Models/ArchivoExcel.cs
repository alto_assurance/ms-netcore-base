using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
public class ArchivoExcel 
{
   [AllowedExtensions(new string[] { ".xls", ".xlsx" })]
   [RequiredAttribute]
   public IFormFile File { get; set; }
}