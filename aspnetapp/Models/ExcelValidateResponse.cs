
using System.Collections.Generic;
using aspnetapp.Models;

namespace aspnetapp{
    public class ExcelValidateResponse 
    {  
        public int Code { get; set; }  
      
        public string Msg { get; set; }

        public string Account { get; set; }
      
        public List<Validation> Errors { get; set; }
      
        public static ExcelValidateResponse GetResult(int code, string msg, List<Validation> errors)  
        {  
            return new ExcelValidateResponse
            {  
                Code = code,  
                Msg = msg,  
                Errors = errors
            }; 
        }
    }
}
  