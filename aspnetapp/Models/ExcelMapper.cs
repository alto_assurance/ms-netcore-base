
namespace aspnetapp.Models{

    public class ExcelMapper{
        public string OriginXLSName { get; set; }
        public string DestinationJsonName { get; set; }
    }
}